const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const { Schema } = mongoose;

const userSchema = new Schema({
    email: String,
    password: String,
});

userSchema.method.encryptPasword = (password)=>{
    //Recibe la contraseña e indicamos cada cuando se ejecute el algoritmo, a más veces más fuertes es
    return bcrypt.hashSync(password,bcrypt.genSaltSync(10));
};

userSchema.methods.comparePassword = function(password){
    return bcrypt.compareSync(password, this.password)

}

module.exports = mongoose.model('users',userSchema);