var express = require('express');
var router = express.Router();
const passport = require('passport');


/* GET home page. */
//req: request
//res: response
//next: next
router.get('/', function(req, res, next) {
  res.render('index', { title: 'The Guide' });
});
router.get('/mision', function(req, res, next) {
  res.render('mision', { title: 'The Guide - Misión' });
});
router.get('/vision', function(req, res, next) {
  res.render('vision', { title: 'The Guide - Visión' });
});
router.get('/contacto', function(req, res, next) {
  res.render('contacto', { title: 'The Guide - Contacto' });
});
router.get('/portfolio', function(req, res, next) {
  res.render('portfolio', { title: 'The Guide - Tienda' });
});
<<<<<<< HEAD
router.get('/signup', function(req, res, next) {
  res.render('signup', { title: 'Karma Shop - Ingresar' });
});
//Para enviar los datos del usuario
router.post('/signup', passport.authenticate('local-signup', {
  successRedirect: '/signin',
  //failureRedirect: '/',
  //Para pasarle internamente la variable req del router.get
  passReqToCallback: true
}));
router.get('/signin', function(req, res, next) {
  res.render('login', { title: 'Karma Shop - Ingresar' });
});
//Para enviar los datos del usuario
router.post('/signin', passport.authenticate('local-signin', {
  successRedirect: '/',
  //failureRedirect: '/',
  //Para pasarle internamente la variable req del router.get
  passReqToCallback: true
}));
router.get('/logout',(req, res, next) =>{
req.logOut();
res.redirect('/');
});
router.get('/create', isAuthenticate, (req, res, next) => {
  res.render('create', { title: 'Karma Shop - Nuevo' });
});
router.get('/edit', isAuthenticate, (req, res, next) => {
  res.render('edit', { title: 'Karma Shop - Editar' });
});
router.get('/delete', isAuthenticate,(req, res, next) => {
  res.render('delete', { title: 'Karma Shop - Borrar' });
});
router.get('/details', isAuthenticate,(req, res, next) => {
  res.render('details', { title: 'Karma Shop - Detalles' });
=======
router.get('/login', function(req, res, next) {
  res.render('login', { title: 'The Guide - Ingresar' });
});
router.get('/create', function(req, res, next) {
  res.render('create', { title: 'The Guide - Nuevo' });
});
router.get('/edit', function(req, res, next) {
  res.render('edit', { title: 'The Guide - Editar' });
});
router.get('/delete', function(req, res, next) {
  res.render('delete', { title: 'The Guide - Borrar' });
});
router.get('/details', function(req, res, next) {
  res.render('details', { title: 'The Guide - Detalles' });
>>>>>>> 8d84a1d18c04a6c0e3d96e334bb5d778cfce2b40
});

//método para autentificar que el usuario este logueado y no pueda acceder al panel administrativo
function isAuthenticate(req, res ,next){
  if(req.isAuthenticatecate()){
    return next();
  }else{
    res.redirect('/signin');
  }
};

module.exports = router;
